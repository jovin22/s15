console.log("Hello World");

/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:




/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	

let nameF = "First Name:" + "jovin"
console.log(nameF)

let nameL = "Last Name: " + "Basco"
console.log(nameL)

let age1 = "Age: " + 28
console.log(age1)

let hobby = ["eat", "sleep", "work"]
console.log(hobby)

let workAddress ={
	houseNumber: 454,
	street : "apitong st",
	City : "makati city",
	State : "NA"
}
console.log(workAddress)

	let fullName = "My Full name is: " + "Steve Rogers";
	console.log(fullName)

	let age = "My current age is: " + 40;
	console.log(age)
	
	let friends = ["Tony","Bruce","Thor","Natasha","Clint", "Nick"];
	console.log("My Friends are: ")
	console.log(friends);

	let profile = {
        username: 'captain america',
		fullName: 'Steve Rogers',
		age: 40,
		isActive: false,

	}
	console.log("My Full Profile: ")
	console.log(profile);

	let fullName2 = "My bestfriend is: " + "Bucky Barnes";
	console.log(fullName2);

	const lastLocation = "I was found frozen in:" + "Atlantic Ocean";
	console.log(lastLocation);

